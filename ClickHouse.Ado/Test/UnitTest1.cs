using ClickHouse.Ado;
using Dapper;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Test
{
    public class Tests
    {
        public IDbConnection GetConn()
        {
            var conn = new ClickHouseConnection("Host=192.168.238.129;Port=9000;User=default;Database=test;Compress=True");
            conn.Open();
            return conn;
        }

        [Test]
        public void Database()
        {
            using var conn = GetConn();
            conn.Execute("create database if not exists test");
            var dbList = conn.Query<string>("show databases"); //show databases like 'name'
            Console.WriteLine(JsonConvert.SerializeObject(dbList));
        }

        [Test]
        public void Table()
        {
            using var conn = GetConn();
            conn.Execute("drop table if exists people");
            //conn.Execute("create table if not exists people(id Int32,name String,time Date)ENGINE=MergeTree(time,(id),8192)");
            conn.Execute("create table if not exists people(id Int32,name String)ENGINE=Log");

            var tableList = conn.Query<string>("show tables"); //show tables like 'name'
            Console.WriteLine(JsonConvert.SerializeObject(tableList));
        }

        [Test]
        public void Insert0()
        {
            using var conn = GetConn();
            var sql = "insert into people values(@id,@name)";
            conn.Execute(sql, new people { id = 1, name = "李四" });
        }

        [Test]
        public void Insert()
        {
            using var conn = GetConn();
            var sql = "insert into people values(@id,@name)";
            conn.Execute(sql, new { id = 1, name = "李四" });
        }

        [Test]
        public void InsertMany()
        {
            using var conn = GetConn();
            
            var list2 = new List<people>();
            for (int i = 0; i < 20; i++)
            {
                list2.Add(new people { id = i, name = "李四" + i });
            }
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var sql2 = "insert into people values @list";
            conn.Execute(sql2, new { list = list2 });
            sw.Stop();
            Console.WriteLine("ms:" + sw.ElapsedMilliseconds);
        }

        [Test]
        public void QueryFirstOrDefault()
        {
            using var conn = GetConn();
            var count = conn.QueryFirstOrDefault("select count(1),sum(id),avg(id) from people");
            Console.WriteLine(count);
        }

        [Test]
        public void Query()
        {
            using var conn = GetConn();
            var data = conn.Query("select * from people where id=@id limit 1", new { id = 8 });
            Console.WriteLine(JsonConvert.SerializeObject(data));
        }

        [Test]
        public void Query0()
        {
            using var conn = GetConn();
            var data = conn.Query("select * from people where id=@id limit 1", new people { id = 8 });
            Console.WriteLine(JsonConvert.SerializeObject(data));
        }

        [Test]
        public void ExecuteScalar()
        {
            using var conn = GetConn();
            var dd = conn.ExecuteScalar<decimal>("select count(1) from people");
            Console.WriteLine(dd);

        }

        [Test]
        public void DateTime1()
        {
            using var conn = GetConn();
            conn.Execute("drop table if exists abc");
            conn.Execute("create table if not exists abc(id Int32,name String,date Date,time DateTime)ENGINE=Log");

            var model = new abc
            {
                id = 1,
                name = "李四",
                date = DateTime.Now.Date,
                time = DateTime.Now

            };

            conn.Execute("insert into abc select @id,@name,@date,@time", model);

            var data = conn.QueryFirstOrDefault<abc>("select * from abc limit 1");

            Console.WriteLine(JsonConvert.SerializeObject(data));

        }

        [Test]
        public void Thread()
        {
            for (int i = 0; i < 100; i++)
            {
                Task.Run(() =>
                {
                    using var conn = GetConn();
                    var count = conn.QueryFirstOrDefault("select count(1),sum(id),avg(id) from people");
                    var data = conn.Query("select * from people where id=1 limit 1", new { id = 20 });
                    var dd = conn.ExecuteScalar<decimal>("select count(1) from people");

                    Console.WriteLine(count);
                    Console.WriteLine(data.Count());
                    Console.WriteLine(dd);

                });
            }
        }

    }
}