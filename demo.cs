﻿using ClickHouse.Ado;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.Json;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            using var conn = new ClickHouseConnection("Host=192.168.238.129;Port=9000;User=default;Database=test;Compress=True;");
            conn.Open();

            //conn.Execute("create database if not exists test");
            var dbList = conn.Query<string>("show databases"); //show databases like 'name'
            Console.WriteLine(JsonSerializer.Serialize(dbList));

            //conn.Execute("drop table if exists people");
            //conn.Execute("create table if not exists people(id Int32,name String,time Date)ENGINE=MergeTree(time,(id),8192)");
            conn.Execute("create table if not exists people(id Int32,name String)ENGINE=Log");

            var tableList = conn.Query<string>("show tables"); //show tables like 'name'
            Console.WriteLine(JsonSerializer.Serialize(tableList));

            //var sql = "insert into people values(@id,@name)";
            //conn.Execute(sql, new { id = 1, name = "李四" });

            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //var list2 = new List<people>();
            //for (int i = 0; i < 10000; i++)
            //{
            //    list2.Add(new people { id = i, name = "李四" + i });
            //}
            //var sql2 = "insert into people(id,name) values @list";
            //conn.Execute(sql2, new { list = list2 });
            //sw.Stop();
            //Console.WriteLine(sw.ElapsedMilliseconds);

            var count = conn.QueryFirstOrDefault("select count(1),sum(id),avg(id) from people");
            Console.WriteLine(count);

            var data = conn.Query("select * from people where id=100 limit 1");
            Console.WriteLine(JsonSerializer.Serialize(data));

            Console.WriteLine("Hello World!");
            Console.ReadKey();
        }

        class people : IEnumerable
        {
            public int id { get; set; }

            public string name { get; set; }

            public IEnumerator GetEnumerator()
            {
                yield return id;
                yield return name;
            }
        }
    }
}
